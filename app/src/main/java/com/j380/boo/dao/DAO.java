package com.j380.boo.dao;

import android.app.DownloadManager;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j380.boo.net.model.ReceivedComment;
import com.j380.boo.net.model.ReceivedDetailedPost;

import java.util.List;

import de.greenrobot.dao.query.Query;
import de.greenrobot.dao.query.QueryBuilder;

public class DAO {

    private SQLiteDatabase mDb;
    private DaoMaster mDaoMaster;
    private DaoSession mDaoSession;
    Context mContext;

    private PostDao mPostDao;
    private CommentDao mCommentDao;

    public DAO(Context pCtx) {
        mContext = pCtx;
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(pCtx, "notes-mDb", null);

        mDb = helper.getWritableDatabase();
        mDaoMaster = new DaoMaster(mDb);
        mDaoSession = mDaoMaster.newSession();
        mPostDao = mDaoSession.getPostDao();
        mCommentDao = mDaoSession.getCommentDao();
    }

    public long save(DaoEntity pEntity) {
        return mDaoSession.insertOrReplace(pEntity);
    }


    public void savePosts(ReceivedDetailedPost[] pPosts) {
        for (ReceivedDetailedPost  lPost : pPosts) {
            save(Post.fromReceivedPost(lPost));
        }
    }

    public void saveComments(ReceivedComment[] pComments, String pPostId) {
        for (ReceivedComment comment : pComments) {
            save(Comment.fromReceivedComment(comment, pPostId));
        }
    }

    public List getPosts(Integer pLimit) {
        QueryBuilder lQueryBuilder =  mPostDao.queryBuilder()
                .orderDesc(PostDao.Properties.Time);
        if (pLimit != null ) {
            lQueryBuilder.limit(pLimit);
        }
        return lQueryBuilder.build()
                .listLazy();
    }

    public List getCommentsForPost(String pPostId) {
        return mCommentDao.queryBuilder()
                .where(CommentDao.Properties.PostId.eq(pPostId))
                .orderDesc(CommentDao.Properties.Time)
                .build()
                .listLazy();
    }


}
