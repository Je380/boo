package com.j380.boo.dao;

import android.util.Log;

import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.google.gson.Gson;
import com.j380.boo.BooApplication;
import com.j380.boo.net.model.ReceivedComment;
import com.j380.boo.net.model.ReceivedDetailedPost;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

/**
 * Helper class, static methods only
 */
public class DataHelper {

    public static void parseAndSavePosts(GraphResponse pResponse) {
        JSONObject lJSONResp = pResponse.getJSONObject();
        String lResponse = null;
        try {
            Log.d("Data posts", "Received :" + lJSONResp + (pResponse.getError() == null ? "" : pResponse.getError()));
            lResponse = lJSONResp.getString("data");

        } catch (JSONException e) {
            e.printStackTrace();
            return;
        } catch (NullPointerException e) {
            e.printStackTrace();
            return;
        }

        ReceivedDetailedPost[] lPosts = new Gson().fromJson(lResponse, ReceivedDetailedPost[].class);
        BooApplication.getDao().savePosts(lPosts);
    }

    public static void parseAndSaveComment(GraphResponse pResponse, String pPostId) {
        JSONObject lJSONObject = pResponse.getJSONObject();
        String lResp = null;
        try {
            Log.d("Data comment", "Received :" + lJSONObject +  (pResponse.getError() == null ? "" : pResponse.getError()));
            lResp = lJSONObject.getString("data");
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }
        ReceivedComment[] lReceivedComments = new Gson().fromJson(lResp, ReceivedComment[].class);
        BooApplication.getDao().saveComments(lReceivedComments, pPostId);
    }

    public static String parseAndGetNextCommentPaging(GraphResponse pResponse) {
        JSONObject lJSONObject = pResponse.getJSONObject();
        try {
            return lJSONObject.getJSONObject("paging").getJSONObject("cursors").getString("before");
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

}
