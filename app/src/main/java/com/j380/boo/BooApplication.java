package com.j380.boo;

import android.app.Application;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.FacebookSdk;
import com.j380.boo.dao.DAO;
import com.j380.boo.util.Helper;
import com.squareup.picasso.Picasso;

import net.danlew.android.joda.JodaTimeAndroid;


public class BooApplication extends Application {


    private static final String TAG = BooApplication.class.getSimpleName();

    private static BooApplication sInstance;

    private AccessToken mToken;

    private DAO mDao;

    @Override
    public void onCreate() {
        super.onCreate();
        FacebookSdk.sdkInitialize(this);

        sInstance = this;
        mDao = new DAO(this);
        mToken = AccessToken.getCurrentAccessToken();

        AccessTokenTracker accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(
                    AccessToken oldAccessToken,
                    AccessToken currentAccessToken) {
                updateWithToken(currentAccessToken);
            }
        };

        accessTokenTracker.startTracking();

        JodaTimeAndroid.init(this);

        Picasso.with(this).setLoggingEnabled(true);
    }

    private void updateWithToken(AccessToken token) {
        mToken = token;
        Helper.saveIsLogged(this, token);
    }

    public static AccessToken getToken() {
        return sInstance.mToken;
    }

    public static DAO getDao() {
        return sInstance.mDao;
    }

    public static final BooApplication getInstance() {
        return sInstance;
    }


}
