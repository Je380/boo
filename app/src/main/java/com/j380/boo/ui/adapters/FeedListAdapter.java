package com.j380.boo.ui.adapters;

import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.j380.boo.BooApplication;
import com.j380.boo.R;
import com.j380.boo.dao.Post;
import com.j380.boo.ui.DetailedActivity;
import com.j380.boo.ui.FeedActivity;
import com.j380.boo.util.Helper;
import com.squareup.picasso.Picasso;

import java.util.List;

public class FeedListAdapter extends BaseAdapter {

    private Context mContext;
    private List<Post> mData;
    private long mLastPostTime;
    private int POST_LIMIT_STEP = 5;
    private int mPostLimit;

    public FeedListAdapter(Context pContext) {
        mData = BooApplication.getDao().getPosts(mPostLimit+=POST_LIMIT_STEP);
        mContext = pContext;
        mLastPostTime = mData.isEmpty() ? 0 : mData.get(mData.size() - 1).getTime().getTime();
    }

    @Override
    public int getCount() {
        return mData == null? 0 : mData.size();
    }

    @Override
    public Post getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View newView() {
        LayoutInflater lInflater = LayoutInflater.from(mContext);
        View lView =  lInflater.inflate(R.layout.feed_element_layout, null);
        lView.setTag(new Holder(lView));
        return lView;
    }

    public void bindView(View pView, int position) {
        ((Holder)pView.getTag()).bindData(getItem(position));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v;
        if (convertView == null) {
            v = newView();
        } else {
            v = convertView;
        }
        bindView(v, position);
        return v;
    }

    public void reload() {
        mData = BooApplication.getDao().getPosts(mPostLimit+=POST_LIMIT_STEP);
        mLastPostTime = mData.isEmpty() ? 0 : mData.get(mData.size() - 1).getTime().getTime();
        notifyDataSetChanged();
    }

    public long getLastPostTime() {
        return mLastPostTime;
    }

    public static class Holder {

        private View mView;
        private TextView mMessage;
        private TextView mDate;
        private TextView mCommentsCount;
        private ImageView mImage;
        private ImageView mAvatar;

        public Holder(View pView) {
            mView = pView;
            mMessage = (TextView) mView.findViewById(R.id.text);
            mDate = (TextView) mView.findViewById(R.id.time);
            mCommentsCount = (TextView) mView.findViewById(R.id.number_of_comments);
            mImage = (ImageView) mView.findViewById(R.id.image);
            mAvatar = (ImageView) mView.findViewById(R.id.avatar);
            Picasso.with(mAvatar.getContext())
                    .load(FeedActivity.PHOTO_LINK)
                    .placeholder(ActivityCompat.getDrawable(mImage.getContext(), R.drawable.placeholder_ava))
                    .into(mAvatar);

        }

        public void bindData(final Post pPost) {
            mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DetailedActivity.start(mView.getContext(), pPost);
                }
            });
            mMessage.setText(pPost.getMessage());
            mDate.setText(Helper.getTimeForPost(pPost.getTime(), mDate.getContext()));
            mCommentsCount.setText(String.format(mCommentsCount.getContext().getString(R.string.comment_number_template),
                    pPost.getCommentCount().toString()));
            String lPhotoUrl = pPost.getPhotoUrl();
            if (!TextUtils.isEmpty(lPhotoUrl)) {
                Picasso.with(mImage.getContext())
                        .load(lPhotoUrl)
                        .placeholder(ActivityCompat.getDrawable(mImage.getContext(), R.drawable.placeholder))
                        .into(mImage);
            }

        }
    }
}
