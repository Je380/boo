package com.j380.boo.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.j380.boo.R;
import com.j380.boo.util.Helper;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class LoginActivity extends Activity {

    private LoginButton mLoginButton;
    private CallbackManager mCallbackManager;
    private static final String TAG = LoginActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mCallbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_login_layout);



//        try {
//            PackageInfo info = getPackageManager().getPackageInfo(
//                    getApplicationContext().getPackageName(),
//                    PackageManager.GET_SIGNATURES);
//            for (Signature signature : info.signatures) {
//                MessageDigest md = MessageDigest.getInstance("SHA");
//                md.update(signature.toByteArray());
//                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
//            }
//        } catch (PackageManager.NameNotFoundException e) {
//            Log.e(TAG, "name not found: " + e.getMessage());
//
//        } catch (NoSuchAlgorithmException e) {
//            Log.e(TAG, "no alg" + e.getMessage());
//        }

        mLoginButton = (LoginButton) findViewById(R.id.login_button);

        mCallbackManager = CallbackManager.Factory.create();

        mLoginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult pLoginResult) {
                LoginActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        FeedActivity.start(LoginActivity.this);
                        finish();
                        Helper.saveIsLogged(LoginActivity.this, pLoginResult.getAccessToken());
                        Log.e(TAG, "ok");
                    }
                });
            }

            @Override
            public void onCancel() {                ;
                Toast.makeText(LoginActivity.this, "Login error", Toast.LENGTH_SHORT).show();
                Log.d(TAG, "Login canceled");
            }

            @Override
            public void onError(FacebookException e) {

                Toast.makeText(LoginActivity.this, "Login error", Toast.LENGTH_SHORT).show();
                Log.d(TAG, "Login error");
                e.printStackTrace();

            }
        });
    }

    public static void start(Context pContext) {
        Intent lIntent = new Intent(pContext, LoginActivity.class);
        pContext.startActivity(lIntent);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
