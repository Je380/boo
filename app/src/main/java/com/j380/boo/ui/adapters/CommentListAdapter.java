package com.j380.boo.ui.adapters;

import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.j380.boo.BooApplication;
import com.j380.boo.R;
import com.j380.boo.dao.Comment;
import com.j380.boo.dao.Post;
import com.j380.boo.ui.FeedActivity;
import com.j380.boo.util.Helper;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CommentListAdapter extends BaseAdapter {

    private Context mContext;
    private List<Comment> mData;
    private String mPostId;

    public CommentListAdapter(Context pContext, String pPostId) {
        mData = BooApplication.getDao().getCommentsForPost(pPostId);
        mContext = pContext;
        mPostId = pPostId;
    }

    @Override
    public int getCount() {
        return mData == null? 0 : mData.size();
    }

    @Override
    public Comment getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View newView() {
        LayoutInflater lInflater = LayoutInflater.from(mContext);
        View lView =  lInflater.inflate(R.layout.comment_element_layout, null);
        lView.setTag(new Holder(lView));
//        lView.setClickable(false);
        lView.setEnabled(false);
        return lView;
    }

    public void bindView(View pView, int position) {
        ((Holder)pView.getTag()).bindData(getItem(position));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v;
        if (convertView == null) {
            v = newView();
        } else {
            v = convertView;
        }
        bindView(v, position);
        return v;
    }

    public void reload() {
        mData = BooApplication.getDao().getCommentsForPost(mPostId);
        notifyDataSetChanged();
    }

    public static class Holder {

        private View mView;
        private TextView mMessage;
        private TextView mDate;
        private TextView mTitle;
        private ImageView mAvatar;

        public Holder(View pView) {
            mView = pView;
            mMessage = (TextView) mView.findViewById(R.id.text_comment);
            mDate = (TextView) mView.findViewById(R.id.time_comment);
            mTitle = (TextView) mView.findViewById(R.id.name_comment);
            mAvatar = (ImageView) mView.findViewById(R.id.avatar_comment);
        }

        public void bindData(Comment pComment) {
            mTitle.setText(pComment.getName());
            mMessage.setText(pComment.getMessage());
            mDate.setText(Helper.getTimeForPost(pComment.getTime(), mDate.getContext()));
            Picasso.with(mAvatar.getContext())
                    .load(String.format(mAvatar.getContext().getString(R.string.profile_picture_url_template),
                            pComment.getUserId()))
                    .placeholder(ActivityCompat.getDrawable(mAvatar.getContext(), R.drawable.placeholder_ava))
                    .into(mAvatar);

        }
    }
}
