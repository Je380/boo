package com.j380.boo.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;

import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.j380.boo.BooApplication;
import com.j380.boo.R;
import com.j380.boo.dao.DataHelper;
import com.j380.boo.ui.adapters.FeedListAdapter;
import com.j380.boo.util.Helper;


public class FeedActivity extends AppCompatActivity {

    public static final String TAG = FeedActivity.class.getSimpleName();

    private ListView mList;
    private FeedListAdapter mAdapter;
    public static final String MAIN_LINK = "80329313253/feed";
    public static final String PHOTO_LINK = "https://graph.facebook.com/80329313253/picture?width=100&height=100";

    private SwipeRefreshLayout mSwipeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_layout);

        //checks if user is logged and opens LoginActivity if not
        if (!Helper.getIsLogged(this)) {
            LoginActivity.start(this);
            Log.e(TAG, "finishing");
            finish();
            return;
        }

        mSwipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe);
        mSwipeLayout.setOnRefreshListener(mSwipeListener);

        mList = (ListView) findViewById(R.id.feed_list);

        mAdapter = new FeedListAdapter(this);
        mList.setAdapter(mAdapter);

        LayoutInflater lInflater = LayoutInflater.from(this);
        View lView = lInflater.inflate(R.layout.loadmore_footer_view_layout, null);
        lView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFeedData(mAdapter.getLastPostTime());
            }
        });
        mList.addFooterView(lView);
        getFeedData(null);
    }

    SwipeRefreshLayout.OnRefreshListener mSwipeListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            getFeedData(null);
            mSwipeLayout.setRefreshing(false);
        }
    };

    public static void start(Context pContext) {
        Intent lIntent = new Intent(pContext, FeedActivity.class);
        pContext.startActivity(lIntent);
    }

    public void getFeedData(Long pDate) {
        Bundle bun = new Bundle();
        bun.putString("fields", "attachments,message,created_time,comments.limit(0).summary(true)");
        bun.putString("limit", "25");
        if (pDate != null) {
            //convert milliseconds to seconds
            bun.putLong("until", pDate / 1000);
        }
        new GraphRequest(
                BooApplication.getToken(),
                MAIN_LINK,
                bun,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        DataHelper.parseAndSavePosts(response);
                        FeedActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mAdapter.reload();
                            }
                        });
                    }
                }
        ).executeAsync();
    }

}
