package com.j380.boo.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.j380.boo.R;
import com.j380.boo.dao.DataHelper;
import com.j380.boo.dao.Post;
import com.j380.boo.net.model.ReceivedDetailedPost;
import com.j380.boo.ui.adapters.CommentListAdapter;
import com.j380.boo.util.Helper;
import com.squareup.picasso.Picasso;

public class DetailedActivity extends AppCompatActivity {

    private ListView mList;
    private CommentListAdapter mAdapter;
    private Post mPost;

    public static final String POST_KEY = "post_key_intent";

    private TextView mMessage;
    private TextView mDate;
    private TextView mCommentsCount;
    private ImageView mImage;
    private ImageView mAvatar;
    private String mAfter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_detail_layout);


        Intent lIntent = getIntent();
        if (lIntent != null) {
            mPost = (Post) lIntent.getSerializableExtra(POST_KEY);
        } else {
            finish();
        }

        mList = (ListView) findViewById(R.id.comment_list);

        mAdapter = new CommentListAdapter(this, mPost.getId());
        mList.setAdapter(mAdapter);
        LayoutInflater lInflater = LayoutInflater.from(this);
        View lView = lInflater.inflate(R.layout.feed_element_layout, null);

        mList.addHeaderView(lView);

        View lFooter = lInflater.inflate(R.layout.loadmore_footer_view_layout, null);
        lFooter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getCommentData();
            }
        });
        mList.addFooterView(lFooter);
        bindHeaderData(lView);
        getCommentData();

    }



    public void bindHeaderData(View pView) {
        mMessage = (TextView) pView.findViewById(R.id.text);
        mDate = (TextView) pView.findViewById(R.id.time);
        mCommentsCount = (TextView) pView.findViewById(R.id.number_of_comments);
        mImage = (ImageView) pView.findViewById(R.id.image);
        mAvatar = (ImageView) pView.findViewById(R.id.avatar);
        Picasso.with(mAvatar.getContext())
                .load(FeedActivity.PHOTO_LINK)
                .placeholder(ActivityCompat.getDrawable(mImage.getContext(), R.drawable.placeholder_ava))
                .into(mAvatar);


        mMessage.setText(mPost.getMessage());
        mDate.setText(Helper.getTimeForPost(mPost.getTime(), mDate.getContext()));
        mCommentsCount.setText(String.format(mCommentsCount.getContext().getString(R.string.comment_number_template),
                mPost.getCommentCount().toString()));
        String lPhotoUrl = mPost.getPhotoUrl();
        if (!TextUtils.isEmpty(lPhotoUrl)) {
            Picasso.with(mImage.getContext())
                    .load(lPhotoUrl)
                    .placeholder(ActivityCompat.getDrawable(mImage.getContext(), R.drawable.placeholder))
                    .into(mImage);
        }
    }

    public void getCommentData() {
        Bundle lBundle = new Bundle();
        lBundle.putString("limit","2");
        lBundle.putString("order","chronological");
        if (!TextUtils.isEmpty(mAfter)) {
            lBundle.putString("after", mAfter);
        }
        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                String.format(getString(R.string.comment_post_template), mPost.getId()),
                lBundle,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        DataHelper.parseAndSaveComment(response, mPost.getId());
                        mAdapter.reload();
                        mAfter = DataHelper.parseAndGetNextCommentPaging(response);

                    }
                }
        ).executeAsync();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static void start(Context pContext, Post pPost) {
        Intent lIntent = new Intent(pContext, DetailedActivity.class);
        lIntent.putExtra(POST_KEY, pPost);
        pContext.startActivity(lIntent);

    }
}
