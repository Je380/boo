package com.j380.boo.net.model;


import com.google.gson.annotations.SerializedName;

public class ReceivedComment {

    @SerializedName("from")
    From mFrom;

    @SerializedName("message")
    String mMessage;

    @SerializedName("created_time")
    String mTime;

    @SerializedName("id")
    String mId;

    public static class From {

        @SerializedName("id")
        String mId;


        @SerializedName("name")
        String mName;

        public String getId() {
            return mId;
        }

        public String getName() {
            return mName;
        }
    }

    public From getFrom() {
        return mFrom;
    }

    public String getMessage() {
        return mMessage;
    }

    public String getTime() {
        return mTime;
    }

    public String getId() {
        return mId;
    }
}
