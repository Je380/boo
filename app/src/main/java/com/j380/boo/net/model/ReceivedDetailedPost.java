package com.j380.boo.net.model;

import android.util.Log;

import com.google.gson.annotations.SerializedName;

public class ReceivedDetailedPost {

    @SerializedName("id")
    String mId;

    @SerializedName("attachments")
    Attachments mAttachments;

    @SerializedName("created_time")
    String mTime;

    @SerializedName("message")
    String mMessage;

    @SerializedName("comments")
    Comments mComments;




    public String getId() {
        return mId;
    }

    public Attachments getAttachments() {
        return mAttachments;
    }

    public static class Attachments {

        @SerializedName("data")
        Data[] mData;

        public Data[] getData() {
            return mData;
        }

        public static class Data {

            @SerializedName("media")
            Media mMedia;

            public Media getMedia() {
                return mMedia;
            }


            public static class Media {

                @SerializedName("image")
                Image mImage;

                public Image getImage() {
                    return mImage;
                }

                public static class Image {

                    @SerializedName("src")
                    String mSrc;

                    @SerializedName("height")
                    String mHeight;

                    @SerializedName("width")
                    String mWidth;

                    public String getSrc() {
                        return mSrc;
                    }

                    public String getHeight() {
                        return mHeight;
                    }

                    public String getWidth() {
                        return mWidth;
                    }
                }

            }
        }
    }


    public String getPhotoUrl() {
        try {
            return mAttachments.getData()[0].getMedia().getImage().getSrc();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getMessage() {
        return mMessage;
    }

    public String getTime() {
        return mTime;
    }

    public Long getCommentsCount() {
        return mComments.getSummary().getTotalCount();
    }

    public static class Comments {

        @SerializedName("summary")
        Summary mSummary;

        public Summary getSummary() {
            return mSummary;
        }

        public static class Summary {

            @SerializedName("total_count")
            Long mTotalCount;

            public Long getTotalCount() {
                return mTotalCount;
            }
        }


    }

}
