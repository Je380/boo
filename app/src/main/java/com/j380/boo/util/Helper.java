package com.j380.boo.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.facebook.AccessToken;
import com.j380.boo.R;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.Period;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Helper class ,static methods only
 */
public class Helper {

    private Helper() {}

    public static void saveIsLogged(Context pContext, AccessToken pAccessToken) {
        SharedPreferences lSharedPreferences = PreferenceManager.getDefaultSharedPreferences(pContext);
        SharedPreferences.Editor lEditor = lSharedPreferences.edit();
        lEditor.putBoolean(pContext.getString(R.string.is_logged_key), pAccessToken != null);
        lEditor.commit();
    }

    public static boolean getIsLogged(Context pContext) {
        SharedPreferences lSharedPreferences = PreferenceManager.getDefaultSharedPreferences(pContext);
        return lSharedPreferences.getBoolean(pContext.getString(R.string.is_logged_key), false);
    }

    public static Date parseDate(String pDate) {
        SimpleDateFormat incomingFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        try {
            return incomingFormat.parse(pDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getTimeForPost(Date pDate, Context pContext) {
        LocalDate lToday = LocalDate.now();
        LocalDate lLocalDate = new LocalDate(pDate);
        //the same year ?
        if (lLocalDate.getYear() == lToday.getYear()) {
            // the same month and day
            if (lLocalDate.getMonthOfYear() == lToday.getMonthOfYear() && lLocalDate.getDayOfMonth() == lToday.getDayOfMonth()) {
                Period period = new Period(new DateTime(System.currentTimeMillis()), new DateTime(pDate));
                int hours = period.getHours();
                if (hours != 0) {
                    return String.format(pContext.getString(R.string.date_hours_template), hours);
                } else {
                    int min = period.getMinutes();
                    if (min != 0) {
                        return String.format(pContext.getString(R.string.date_minute_template), min);
                    } else {
                        return pContext.getString(R.string.date_just_now);
                    }
                }
            } else {
                Calendar lCalendar = Calendar.getInstance();
                lCalendar.setTime(pDate);
                return String.format(pContext.getString(R.string.date_template), lCalendar);
            }
        } else {
            Calendar lCalendar = Calendar.getInstance();
            lCalendar.setTime(pDate);
            return String.format(pContext.getString(R.string.date_template_with_year), lCalendar);
        }

    }
}
