package com.j380;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;

public class Generator {
    public static void main(String[] args) throws Exception {
        Schema schema = new Schema(20, "com.j380.boo.dao");
        Entity post = addPost(schema);
        Entity comment = addComment(schema);

        Property postIdProperty = comment.addStringProperty("postId").getProperty();
        comment.addToOne(post, postIdProperty);

        new DaoGenerator().generateAll(schema, "app/src/main/java");
    }

    private static Entity addPost(Schema schema) {
        Entity lRecord = schema.addEntity("Post");
        lRecord.implementsInterface("DaoEntity");
        lRecord.setHasKeepSections(true);
        lRecord.implementsSerializable();
        lRecord.addStringProperty("id").primaryKey();
        lRecord.addStringProperty("message");
        lRecord.addStringProperty("photoUrl");
        lRecord.addLongProperty("commentCount");
        lRecord.addStringProperty("rawTime");
        lRecord.addDateProperty("time");
        return lRecord;
    }

    private static Entity addComment(Schema schema) {
        Entity lRecord = schema.addEntity("Comment");
        lRecord.implementsInterface("DaoEntity");
        lRecord.setHasKeepSections(true);
        lRecord.implementsSerializable();
        lRecord.addStringProperty("id").primaryKey();
        lRecord.addStringProperty("message");
        lRecord.addStringProperty("userId");
        lRecord.addDateProperty("time");
        lRecord.addStringProperty("rawTime");
        lRecord.addStringProperty("name");
        return lRecord;
    }
}
